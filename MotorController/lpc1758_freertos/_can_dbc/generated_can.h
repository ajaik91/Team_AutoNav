/// DBC file: ../_can_dbc/243.dbc    Self node: 'MOTOR'  (ALL = 0)
/// This file can be included by a source file, for example: #include "generated.h"
#ifndef __GENEARTED_DBC_PARSER
#define __GENERATED_DBC_PARSER
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>



/// Extern function needed for dbc_encode_and_send()
extern bool dbc_app_send_can_msg(uint32_t mid, uint8_t dlc, uint8_t bytes[8]);

/// Missing in Action structure
typedef struct {
    uint32_t is_mia : 1;          ///< Missing in action flag
    uint32_t mia_counter_ms : 31; ///< Missing in action counter
} dbc_mia_info_t;

/// CAN message header structure
typedef struct { 
    uint32_t mid; ///< Message ID of the message
    uint8_t  dlc; ///< Data length of the message
} dbc_msg_hdr_t; 

static const dbc_msg_hdr_t KILL_SWITCH_HDR =                      {  100, 1 };
// static const dbc_msg_hdr_t STOP_CMD_HDR =                         {  110, 1 };
static const dbc_msg_hdr_t MOTOR_SYNC_HDR =                       {  120, 1 };
// static const dbc_msg_hdr_t GEO_SYNC_HDR =                         {  130, 1 };
// static const dbc_msg_hdr_t BLUETOOTH_SYNC_CMD_HDR =               {  140, 1 };
static const dbc_msg_hdr_t SENSOR_SYNC_HDR =                      {  150, 1 };
static const dbc_msg_hdr_t ERROR_COMM_DRIVER_HDR =                {  160, 1 };
static const dbc_msg_hdr_t DRIVER_SYNC_ACK_HDR =                  {  170, 1 };
static const dbc_msg_hdr_t MOTOR_CONTROL_HDR =                    {  180, 2 };
static const dbc_msg_hdr_t SENSOR_SONAR_HDR =                     {  190, 5 };
// static const dbc_msg_hdr_t GEO_SEND_LOCATION_HDR =                {  200, 8 };
static const dbc_msg_hdr_t GEO_SEND_HD_BR_HDR =                   {  210, 5 };
// static const dbc_msg_hdr_t NAVIGATION_WAYPOINT_HDR =              {  220, 8 };
// static const dbc_msg_hdr_t START_CMD_HDR =                        {  230, 1 };
static const dbc_msg_hdr_t SENSOR_BATTERY_HDR =                   {  240, 8 };
static const dbc_msg_hdr_t DRIVER_TEST_HDR =                      {  500, 1 };
static const dbc_msg_hdr_t MOTOR_TEST_HDR =                       {  600, 1 };
static const dbc_msg_hdr_t SENSOR_TEST_HDR =                      {  700, 1 };
static const dbc_msg_hdr_t GEO_TEST_HDR =                         {  800, 1 };
static const dbc_msg_hdr_t BLUETOOTH_TEST_HDR =                   {  900, 1 };

/// Enumeration(s) for Message: 'MOTOR_CONTROL' from 'DRIVER'
typedef enum {
    right = 4,
    sleft = 2,
    straight = 0,
    left = 1,
    sright = 3,
} steer_E ;

typedef enum {
    normal = 2,
    fast = 3,
    slow = 1,
    stop = 0,
} speed_E ;




/// Message: KILL_SWITCH from 'DRIVER', DLC: 1 byte(s), MID: 100
typedef struct {
    uint8_t killSwitch : 1;                   ///< B0:0   Destination: MOTOR

    dbc_mia_info_t mia_info;
} KILL_SWITCH_t;


/// Message: MOTOR_SYNC from 'MOTOR', DLC: 1 byte(s), MID: 120
typedef struct {
    uint8_t Motor_Sync : 1;                   ///< B0:0   Destination: DRIVER

    // No dbc_mia_info_t for a message that we will send
} MOTOR_SYNC_t;


/// Message: SENSOR_SYNC from 'SENSOR', DLC: 1 byte(s), MID: 150
typedef struct {
    uint8_t Sensor_Sync : 1;                  ///< B0:0   Destination: DRIVER,MOTOR,BLUETOOTH

    dbc_mia_info_t mia_info;
} SENSOR_SYNC_t;


/// Message: ERROR_COMM_DRIVER from 'DRIVER', DLC: 1 byte(s), MID: 160
typedef struct {
    uint8_t Sync_Miss_Info;                   ///< B7:0   Destination: MOTOR

    dbc_mia_info_t mia_info;
} ERROR_COMM_DRIVER_t;


/// Message: DRIVER_SYNC_ACK from 'DRIVER', DLC: 1 byte(s), MID: 170
typedef struct {
    uint8_t Sync_Miss_Info : 1;               ///< B0:0   Destination: MOTOR,BLUETOOTH,SENSOR,GEO

    dbc_mia_info_t mia_info;
} DRIVER_SYNC_ACK_t;


/// Message: MOTOR_CONTROL from 'DRIVER', DLC: 2 byte(s), MID: 180
typedef struct {
    steer_E steer;                            ///< B7:0   Destination: MOTOR
    speed_E speed;                            ///< B15:8   Destination: MOTOR

    dbc_mia_info_t mia_info;
} MOTOR_CONTROL_t;


/// Message: SENSOR_SONAR from 'SENSOR', DLC: 5 byte(s), MID: 190
typedef struct {
    uint8_t Sensor_Sonar_Front;               ///< B7:0   Destination: DRIVER,MOTOR,BLUETOOTH
    uint8_t Sensor_Sonar_FrontRight;          ///< B15:8   Destination: DRIVER,MOTOR,BLUETOOTH
    uint8_t Sensor_Sonar_FrontLeft;           ///< B23:16   Destination: DRIVER,MOTOR,BLUETOOTH
    uint8_t Sensor_Sonar_Back;                ///< B31:24   Destination: DRIVER,MOTOR,BLUETOOTH

    dbc_mia_info_t mia_info;
} SENSOR_SONAR_t;


/// Message: GEO_SEND_HD_BR from 'GEO', DLC: 5 byte(s), MID: 210
typedef struct {
    uint8_t Geo_speed;                        ///< B7:0   Destination: DRIVER,MOTOR
    uint16_t Geo_heading;                     ///< B23:8   Destination: DRIVER,MOTOR
    uint16_t Geo_bearing;                     ///< B39:24   Destination: DRIVER,MOTOR

    dbc_mia_info_t mia_info;
} GEO_SEND_HD_BR_t;


/// Message: SENSOR_BATTERY from 'SENSOR', DLC: 8 byte(s), MID: 240
typedef struct {
    float Sensor_Battery_Voltage;             ///< B7:0   Destination: DRIVER,MOTOR,BLUETOOTH
    float Sensor_Battery_SOC;                 ///< B17:8   Destination: DRIVER,MOTOR,BLUETOOTH
    float Sensor_Light;                       ///< B27:18   Destination: DRIVER,MOTOR,BLUETOOTH
    float Sensor_Tilt_X;                      ///< B39:28   Destination: DRIVER,MOTOR,BLUETOOTH
    float Sensor_Tilt_Y;                      ///< B51:40   Destination: DRIVER,MOTOR,BLUETOOTH

    dbc_mia_info_t mia_info;
} SENSOR_BATTERY_t;


/// Message: DRIVER_TEST from 'DRIVER', DLC: 1 byte(s), MID: 500
typedef struct {
    uint8_t Driver_Test_Msg;                  ///< B7:0   Destination: MOTOR,GEO,SENSOR,BLUETOOTH

    dbc_mia_info_t mia_info;
} DRIVER_TEST_t;


/// Message: MOTOR_TEST from 'MOTOR', DLC: 1 byte(s), MID: 600
typedef struct {
    uint8_t Motor_Test_Msg;                   ///< B7:0   Destination: DRIVER,GEO,SENSOR,BLUETOOTH

    // No dbc_mia_info_t for a message that we will send
} MOTOR_TEST_t;


/// Message: SENSOR_TEST from 'SENSOR', DLC: 1 byte(s), MID: 700
typedef struct {
    uint8_t Sensor_Test_Msg;                  ///< B7:0   Destination: DRIVER,MOTOR,GEO,BLUETOOTH

    dbc_mia_info_t mia_info;
} SENSOR_TEST_t;


/// Message: GEO_TEST from 'GEO', DLC: 1 byte(s), MID: 800
typedef struct {
    uint8_t Geo_Test_Msg;                     ///< B7:0   Destination: DRIVER,MOTOR,SENSOR,BLUETOOTH

    dbc_mia_info_t mia_info;
} GEO_TEST_t;


/// Message: BLUETOOTH_TEST from 'BLUETOOTH', DLC: 1 byte(s), MID: 900
typedef struct {
    uint8_t Bluetooth_Test_Msg;               ///< B7:0   Destination: DRIVER,MOTOR,GEO,SENSOR

    dbc_mia_info_t mia_info;
} BLUETOOTH_TEST_t;


/// @{ These 'externs' need to be defined in a source file of your project
extern const uint32_t                             KILL_SWITCH__MIA_MS;
extern const KILL_SWITCH_t                        KILL_SWITCH__MIA_MSG;
extern const uint32_t                             SENSOR_SYNC__MIA_MS;
extern const SENSOR_SYNC_t                        SENSOR_SYNC__MIA_MSG;
extern const uint32_t                             ERROR_COMM_DRIVER__MIA_MS;
extern const ERROR_COMM_DRIVER_t                  ERROR_COMM_DRIVER__MIA_MSG;
extern const uint32_t                             DRIVER_SYNC_ACK__MIA_MS;
extern const DRIVER_SYNC_ACK_t                    DRIVER_SYNC_ACK__MIA_MSG;
extern const uint32_t                             MOTOR_CONTROL__MIA_MS;
extern const MOTOR_CONTROL_t                      MOTOR_CONTROL__MIA_MSG;
extern const uint32_t                             SENSOR_SONAR__MIA_MS;
extern const SENSOR_SONAR_t                       SENSOR_SONAR__MIA_MSG;
extern const uint32_t                             GEO_SEND_HD_BR__MIA_MS;
extern const GEO_SEND_HD_BR_t                     GEO_SEND_HD_BR__MIA_MSG;
extern const uint32_t                             SENSOR_BATTERY__MIA_MS;
extern const SENSOR_BATTERY_t                     SENSOR_BATTERY__MIA_MSG;
extern const uint32_t                             DRIVER_TEST__MIA_MS;
extern const DRIVER_TEST_t                        DRIVER_TEST__MIA_MSG;
extern const uint32_t                             SENSOR_TEST__MIA_MS;
extern const SENSOR_TEST_t                        SENSOR_TEST__MIA_MSG;
extern const uint32_t                             GEO_TEST__MIA_MS;
extern const GEO_TEST_t                           GEO_TEST__MIA_MSG;
extern const uint32_t                             BLUETOOTH_TEST__MIA_MS;
extern const BLUETOOTH_TEST_t                     BLUETOOTH_TEST__MIA_MSG;
/// @}


/// Not generating code for dbc_encode_KILL_SWITCH() since the sender is DRIVER and we are MOTOR

/// Not generating code for dbc_encode_STOP_CMD() since the sender is BLUETOOTH and we are MOTOR

/// Encode MOTOR's 'MOTOR_SYNC' message
/// @returns the message header of this message
static inline dbc_msg_hdr_t dbc_encode_MOTOR_SYNC(uint8_t bytes[8], MOTOR_SYNC_t *from)
{
    uint32_t raw;
    bytes[0]=bytes[1]=bytes[2]=bytes[3]=bytes[4]=bytes[5]=bytes[6]=bytes[7]=0;

    raw = ((uint32_t)(((from->Motor_Sync)))) & 0x01;
    bytes[0] |= (((uint8_t)(raw) & 0x01)); ///< 1 bit(s) starting from B0

    return MOTOR_SYNC_HDR;
}

/// Encode and send for dbc_encode_MOTOR_SYNC() message
static inline bool dbc_encode_and_send_MOTOR_SYNC(MOTOR_SYNC_t *from)
{
    uint8_t bytes[8];
    const dbc_msg_hdr_t hdr = dbc_encode_MOTOR_SYNC(bytes, from);
    return dbc_app_send_can_msg(hdr.mid, hdr.dlc, bytes);
}



/// Not generating code for dbc_encode_GEO_SYNC() since the sender is GEO and we are MOTOR

/// Not generating code for dbc_encode_BLUETOOTH_SYNC_CMD() since the sender is BLUETOOTH and we are MOTOR

/// Not generating code for dbc_encode_SENSOR_SYNC() since the sender is SENSOR and we are MOTOR

/// Not generating code for dbc_encode_ERROR_COMM_DRIVER() since the sender is DRIVER and we are MOTOR

/// Not generating code for dbc_encode_DRIVER_SYNC_ACK() since the sender is DRIVER and we are MOTOR

/// Not generating code for dbc_encode_MOTOR_CONTROL() since the sender is DRIVER and we are MOTOR

/// Not generating code for dbc_encode_SENSOR_SONAR() since the sender is SENSOR and we are MOTOR

/// Not generating code for dbc_encode_GEO_SEND_LOCATION() since the sender is GEO and we are MOTOR

/// Not generating code for dbc_encode_GEO_SEND_HD_BR() since the sender is GEO and we are MOTOR

/// Not generating code for dbc_encode_NAVIGATION_WAYPOINT() since the sender is BLUETOOTH and we are MOTOR

/// Not generating code for dbc_encode_START_CMD() since the sender is BLUETOOTH and we are MOTOR

/// Not generating code for dbc_encode_SENSOR_BATTERY() since the sender is SENSOR and we are MOTOR

/// Not generating code for dbc_encode_DRIVER_TEST() since the sender is DRIVER and we are MOTOR

/// Encode MOTOR's 'MOTOR_TEST' message
/// @returns the message header of this message
static inline dbc_msg_hdr_t dbc_encode_MOTOR_TEST(uint8_t bytes[8], MOTOR_TEST_t *from)
{
    uint32_t raw;
    bytes[0]=bytes[1]=bytes[2]=bytes[3]=bytes[4]=bytes[5]=bytes[6]=bytes[7]=0;

    raw = ((uint32_t)(((from->Motor_Test_Msg)))) & 0xff;
    bytes[0] |= (((uint8_t)(raw) & 0xff)); ///< 8 bit(s) starting from B0

    return MOTOR_TEST_HDR;
}

/// Encode and send for dbc_encode_MOTOR_TEST() message
static inline bool dbc_encode_and_send_MOTOR_TEST(MOTOR_TEST_t *from)
{
    uint8_t bytes[8];
    const dbc_msg_hdr_t hdr = dbc_encode_MOTOR_TEST(bytes, from);
    return dbc_app_send_can_msg(hdr.mid, hdr.dlc, bytes);
}



/// Not generating code for dbc_encode_SENSOR_TEST() since the sender is SENSOR and we are MOTOR

/// Not generating code for dbc_encode_GEO_TEST() since the sender is GEO and we are MOTOR

/// Not generating code for dbc_encode_BLUETOOTH_TEST() since the sender is BLUETOOTH and we are MOTOR

/// Decode DRIVER's 'KILL_SWITCH' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_KILL_SWITCH(KILL_SWITCH_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != KILL_SWITCH_HDR.dlc || hdr->mid != KILL_SWITCH_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]) & 0x01)); ///< 1 bit(s) from B0
    to->killSwitch = ((raw));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Not generating code for dbc_decode_STOP_CMD() since 'MOTOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_MOTOR_SYNC() since 'MOTOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_GEO_SYNC() since 'MOTOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_BLUETOOTH_SYNC_CMD() since 'MOTOR' is not the recipient of any of the signals

/// Decode SENSOR's 'SENSOR_SYNC' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_SENSOR_SYNC(SENSOR_SYNC_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != SENSOR_SYNC_HDR.dlc || hdr->mid != SENSOR_SYNC_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]) & 0x01)); ///< 1 bit(s) from B0
    to->Sensor_Sync = ((raw));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Decode DRIVER's 'ERROR_COMM_DRIVER' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_ERROR_COMM_DRIVER(ERROR_COMM_DRIVER_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != ERROR_COMM_DRIVER_HDR.dlc || hdr->mid != ERROR_COMM_DRIVER_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]))); ///< 8 bit(s) from B0
    to->Sync_Miss_Info = ((raw));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Decode DRIVER's 'DRIVER_SYNC_ACK' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_DRIVER_SYNC_ACK(DRIVER_SYNC_ACK_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != DRIVER_SYNC_ACK_HDR.dlc || hdr->mid != DRIVER_SYNC_ACK_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]) & 0x01)); ///< 1 bit(s) from B0
    to->Sync_Miss_Info = ((raw));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Decode DRIVER's 'MOTOR_CONTROL' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_MOTOR_CONTROL(MOTOR_CONTROL_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != MOTOR_CONTROL_HDR.dlc || hdr->mid != MOTOR_CONTROL_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]))); ///< 8 bit(s) from B0
    to->steer = (steer_E)((raw));
    raw  = ((uint32_t)((bytes[1]))); ///< 8 bit(s) from B8
    to->speed = (speed_E)((raw));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Decode SENSOR's 'SENSOR_SONAR' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_SENSOR_SONAR(SENSOR_SONAR_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != SENSOR_SONAR_HDR.dlc || hdr->mid != SENSOR_SONAR_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]))); ///< 8 bit(s) from B0
    to->Sensor_Sonar_Front = ((raw));
    raw  = ((uint32_t)((bytes[1]))); ///< 8 bit(s) from B8
    to->Sensor_Sonar_FrontRight = ((raw));
    raw  = ((uint32_t)((bytes[2]))); ///< 8 bit(s) from B16
    to->Sensor_Sonar_FrontLeft = ((raw));
    raw  = ((uint32_t)((bytes[3]))); ///< 8 bit(s) from B24
    to->Sensor_Sonar_Back = ((raw));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Not generating code for dbc_decode_GEO_SEND_LOCATION() since 'MOTOR' is not the recipient of any of the signals

/// Decode GEO's 'GEO_SEND_HD_BR' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_GEO_SEND_HD_BR(GEO_SEND_HD_BR_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != GEO_SEND_HD_BR_HDR.dlc || hdr->mid != GEO_SEND_HD_BR_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]))); ///< 8 bit(s) from B0
    to->Geo_speed = ((raw));
    raw  = ((uint32_t)((bytes[1]))); ///< 8 bit(s) from B8
    raw |= ((uint32_t)((bytes[2]))) << 8; ///< 8 bit(s) from B16
    to->Geo_heading = ((raw));
    raw  = ((uint32_t)((bytes[3]))); ///< 8 bit(s) from B24
    raw |= ((uint32_t)((bytes[4]))) << 8; ///< 8 bit(s) from B32
    to->Geo_bearing = ((raw));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Not generating code for dbc_decode_NAVIGATION_WAYPOINT() since 'MOTOR' is not the recipient of any of the signals

/// Not generating code for dbc_decode_START_CMD() since 'MOTOR' is not the recipient of any of the signals

/// Decode SENSOR's 'SENSOR_BATTERY' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_SENSOR_BATTERY(SENSOR_BATTERY_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != SENSOR_BATTERY_HDR.dlc || hdr->mid != SENSOR_BATTERY_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]))); ///< 8 bit(s) from B0
    to->Sensor_Battery_Voltage = ((raw * 0.1));
    raw  = ((uint32_t)((bytes[1]))); ///< 8 bit(s) from B8
    raw |= ((uint32_t)((bytes[2]) & 0x03)) << 8; ///< 2 bit(s) from B16
    to->Sensor_Battery_SOC = ((raw * 0.1));
    raw  = ((uint32_t)((bytes[2] >> 2) & 0x3f)); ///< 6 bit(s) from B18
    raw |= ((uint32_t)((bytes[3]) & 0x0f)) << 6; ///< 4 bit(s) from B24
    to->Sensor_Light = ((raw * 0.1));
    raw  = ((uint32_t)((bytes[3] >> 4) & 0x0f)); ///< 4 bit(s) from B28
    raw |= ((uint32_t)((bytes[4]))) << 4; ///< 8 bit(s) from B32
    to->Sensor_Tilt_X = ((raw * 0.1));
    raw  = ((uint32_t)((bytes[5]))); ///< 8 bit(s) from B40
    raw |= ((uint32_t)((bytes[6]) & 0x0f)) << 8; ///< 4 bit(s) from B48
    to->Sensor_Tilt_Y = ((raw * 0.1));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Decode DRIVER's 'DRIVER_TEST' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_DRIVER_TEST(DRIVER_TEST_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != DRIVER_TEST_HDR.dlc || hdr->mid != DRIVER_TEST_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]))); ///< 8 bit(s) from B0
    to->Driver_Test_Msg = ((raw));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Not generating code for dbc_decode_MOTOR_TEST() since 'MOTOR' is not the recipient of any of the signals

/// Decode SENSOR's 'SENSOR_TEST' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_SENSOR_TEST(SENSOR_TEST_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != SENSOR_TEST_HDR.dlc || hdr->mid != SENSOR_TEST_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]))); ///< 8 bit(s) from B0
    to->Sensor_Test_Msg = ((raw));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Decode GEO's 'GEO_TEST' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_GEO_TEST(GEO_TEST_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != GEO_TEST_HDR.dlc || hdr->mid != GEO_TEST_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]))); ///< 8 bit(s) from B0
    to->Geo_Test_Msg = ((raw));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Decode BLUETOOTH's 'BLUETOOTH_TEST' message
/// @param hdr  The header of the message to validate its DLC and MID; this can be NULL to skip this check
static inline bool dbc_decode_BLUETOOTH_TEST(BLUETOOTH_TEST_t *to, const uint8_t bytes[8], const dbc_msg_hdr_t *hdr)
{
    const bool success = true;
    // If msg header is provided, check if the DLC and the MID match
    if (NULL != hdr && (hdr->dlc != BLUETOOTH_TEST_HDR.dlc || hdr->mid != BLUETOOTH_TEST_HDR.mid)) {
        return !success;
    }

    uint32_t raw;
    raw  = ((uint32_t)((bytes[0]))); ///< 8 bit(s) from B0
    to->Bluetooth_Test_Msg = ((raw));

    to->mia_info.mia_counter_ms = 0; ///< Reset the MIA counter

    return success;
}


/// Handle the MIA for DRIVER's KILL_SWITCH message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_KILL_SWITCH(KILL_SWITCH_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= KILL_SWITCH__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = KILL_SWITCH__MIA_MSG;
        msg->mia_info.mia_counter_ms = KILL_SWITCH__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

/// Handle the MIA for SENSOR's SENSOR_SYNC message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_SENSOR_SYNC(SENSOR_SYNC_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= SENSOR_SYNC__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = SENSOR_SYNC__MIA_MSG;
        msg->mia_info.mia_counter_ms = SENSOR_SYNC__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

/// Handle the MIA for DRIVER's ERROR_COMM_DRIVER message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_ERROR_COMM_DRIVER(ERROR_COMM_DRIVER_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= ERROR_COMM_DRIVER__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = ERROR_COMM_DRIVER__MIA_MSG;
        msg->mia_info.mia_counter_ms = ERROR_COMM_DRIVER__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

/// Handle the MIA for DRIVER's DRIVER_SYNC_ACK message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_DRIVER_SYNC_ACK(DRIVER_SYNC_ACK_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= DRIVER_SYNC_ACK__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = DRIVER_SYNC_ACK__MIA_MSG;
        msg->mia_info.mia_counter_ms = DRIVER_SYNC_ACK__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

/// Handle the MIA for DRIVER's MOTOR_CONTROL message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_MOTOR_CONTROL(MOTOR_CONTROL_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= MOTOR_CONTROL__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = MOTOR_CONTROL__MIA_MSG;
        msg->mia_info.mia_counter_ms = MOTOR_CONTROL__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

/// Handle the MIA for SENSOR's SENSOR_SONAR message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_SENSOR_SONAR(SENSOR_SONAR_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= SENSOR_SONAR__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = SENSOR_SONAR__MIA_MSG;
        msg->mia_info.mia_counter_ms = SENSOR_SONAR__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

/// Handle the MIA for GEO's GEO_SEND_HD_BR message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_GEO_SEND_HD_BR(GEO_SEND_HD_BR_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= GEO_SEND_HD_BR__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = GEO_SEND_HD_BR__MIA_MSG;
        msg->mia_info.mia_counter_ms = GEO_SEND_HD_BR__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

/// Handle the MIA for SENSOR's SENSOR_BATTERY message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_SENSOR_BATTERY(SENSOR_BATTERY_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= SENSOR_BATTERY__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = SENSOR_BATTERY__MIA_MSG;
        msg->mia_info.mia_counter_ms = SENSOR_BATTERY__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

/// Handle the MIA for DRIVER's DRIVER_TEST message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_DRIVER_TEST(DRIVER_TEST_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= DRIVER_TEST__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = DRIVER_TEST__MIA_MSG;
        msg->mia_info.mia_counter_ms = DRIVER_TEST__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

/// Handle the MIA for SENSOR's SENSOR_TEST message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_SENSOR_TEST(SENSOR_TEST_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= SENSOR_TEST__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = SENSOR_TEST__MIA_MSG;
        msg->mia_info.mia_counter_ms = SENSOR_TEST__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

/// Handle the MIA for GEO's GEO_TEST message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_GEO_TEST(GEO_TEST_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= GEO_TEST__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = GEO_TEST__MIA_MSG;
        msg->mia_info.mia_counter_ms = GEO_TEST__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

/// Handle the MIA for BLUETOOTH's BLUETOOTH_TEST message
/// @param   time_incr_ms  The time to increment the MIA counter with
/// @returns true if the MIA just occurred
/// @post    If the MIA counter reaches the MIA threshold, MIA struct will be copied to *msg
static inline bool dbc_handle_mia_BLUETOOTH_TEST(BLUETOOTH_TEST_t *msg, uint32_t time_incr_ms)
{
    bool mia_occurred = false;
    const dbc_mia_info_t old_mia = msg->mia_info;
    msg->mia_info.is_mia = (msg->mia_info.mia_counter_ms >= BLUETOOTH_TEST__MIA_MS);

    if (!msg->mia_info.is_mia) { // Not MIA yet, so keep incrementing the MIA counter
        msg->mia_info.mia_counter_ms += time_incr_ms;
    }
    else if(!old_mia.is_mia)   { // Previously not MIA, but it is MIA now
        // Copy MIA struct, then re-write the MIA counter and is_mia that is overwriten
        *msg = BLUETOOTH_TEST__MIA_MSG;
        msg->mia_info.mia_counter_ms = BLUETOOTH_TEST__MIA_MS;
        msg->mia_info.is_mia = true;
        mia_occurred = true;
    }

    return mia_occurred;
}

#endif
